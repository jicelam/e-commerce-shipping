const path = require('path');

const appHttpPort = 50759;
const appHttpsPort = 44386;
const devServerPort = 5532;
module.exports = {
	hosts: {
		appHttp: `localhost:${appHttpPort}`,
		appHttps: `localhost:${appHttpsPort}`,
		devServer: `localhost:${devServerPort}`
	},
	paths: {
		app: '/',
        out: path.resolve('../ECommerceShipping.Api/wwwroot'),
		public: path.resolve('./public'),
		source: path.resolve('./src')
	},
	ports: {
		appHttp: appHttpPort,
		appHttps: appHttpsPort,
		devServer: devServerPort
	}
}