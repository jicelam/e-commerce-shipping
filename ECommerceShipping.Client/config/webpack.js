const path = require('path');

const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

const { paths } = require('./common');

const styleLoaders = [
	{ loader: 'style-loader' },
	{ loader: 'css-loader' }
];

module.exports = {
	entry: {
		index: path.join(paths.source, 'index.ts')
	},
	output: {
		chunkFilename: '[name].bundle.js',
		filename: '[name].js',
		path: paths.out,
		publicPath: paths.app
	},
	resolve: {
		alias: {
			"app": `${paths.source}/app`,
			"components": `${paths.source}/components`,
			"data": `${paths.source}/data`,
			"services": `${paths.source}/services`,
			"views": `${paths.source}/views`
		},
		extensions: ['.js', '.ts', '.tsx']
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				loader: 'tslint-loader',
				enforce: 'pre',
				options: {
					emitErrors: true
				}
			},
			{
				test: /\.tsx?$/,
				use: 'ts-loader'
			},
			{
				test: /\.css$/,
				use: [
					...styleLoaders
				]
			},
			{
				test: /\.scss$/,
				use: [
					...styleLoaders,
                    { loader: 'sass-loader' }
				]
			},
			{
				test: [
					/\.eot$/,
					/\.svg$/,
					/\.ttf$/,
					/\.woff2?$/
				],
				use: {
					loader: 'url-loader',
					options: {
						limit: Infinity
					}
				}
			}
		]
	},
	plugins: [
		new CopyWebpackPlugin([
			{ from: path.join(paths.public, 'favicon.ico'), to: paths.out },
			{ from: path.join(paths.public, 'manifest.json'), to: paths.out },
		]),
		new HtmlWebpackPlugin({
			template: path.join(paths.public, 'index.html')
		})
	]
};