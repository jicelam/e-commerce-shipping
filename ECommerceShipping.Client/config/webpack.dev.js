const webpackMerge = require('webpack-merge');

const base = require('./webpack');

module.exports = webpackMerge(base, {
	mode: 'development',
	devtool: 'inline-source-map'
});