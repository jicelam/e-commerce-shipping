const webpack = require('webpack');
const webpackMerge = require('webpack-merge');

const dev = require('./webpack.dev');
const { hosts, paths, ports } = require('./common');

module.exports = webpackMerge(dev, {
	output: {
		path: paths.public
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin()
	],
	devServer: {
		compress: true,
		contentBase: paths.public,
		historyApiFallback: true,
		hotOnly: true,
		inline: true,
		overlay: false,
		port: ports.devServer,
		proxy: {
			"/api": {
				target: `http://${hosts.appHttp}`,
				cookieDomainRewrite: {
					[hosts.devServer]: hosts.appHttp
				}
			}
		},
		publicPath: paths.app,
		watchContentBase: true,
		watchOptions: {
			ignored: /node_modules/
		}
	}
});