import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { App } from 'app';
import registerServiceWorker from './registerServiceWorker';

const rootElement = document.getElementById('root');

function renderApp(appComponent: typeof App): void {
	ReactDOM.render(
		React.createElement(appComponent),
		rootElement
	);
};

renderApp(App);
registerServiceWorker();

if (module.hot) {
	module.hot.accept('app', () => {
		const NewApp = require("app").default;

		renderApp(NewApp);
	});
}