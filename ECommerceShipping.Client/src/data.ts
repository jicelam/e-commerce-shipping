﻿export interface ProductData {
    estimatedArrival: string;
    id: number;
    inventoryQuantity: number;
    maxBusinessDaysToShip: number;
    name: string;
    shipOnWeekends: boolean;
}