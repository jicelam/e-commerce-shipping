import classnames from 'classnames';
import * as React from 'react';

import './styles.scss';

export interface PrependedFormGroupProps extends React.HTMLAttributes<HTMLElement> {
	text: string;
}

export const PrependedFormGroup = ({ children, className, text, ...props }: PrependedFormGroupProps) => (
	<div
		{...props}
		className={classnames('input-group', className)}
	>
		<div className="input-group-prepend">
			<span className="input-group-text">
				{text}
			</span>
		</div>
		{children}
	</div>
);

export interface DatetimeInputProps extends React.InputHTMLAttributes<HTMLInputElement> {}

export class DatetimeInput extends React.Component<DatetimeInputProps> {
	private _inputElem: HTMLInputElement | null = null;
	private _normalizedDefaultValue: string | string[] | undefined = undefined;
	
	get value(): string | null { return this._inputElem ? this._inputElem.value : null; }

	constructor(props: DatetimeInputProps) {
		super(props);
		
		const { defaultValue } = props;
		if (defaultValue) {
			this._normalizedDefaultValue = typeof defaultValue === 'string'
				? this.normalizeDateValue(defaultValue)
				: defaultValue
					.map(value => this.normalizeDateValue(value))
					.filter(value => value !== undefined) as string[];
		}
	}

	render(): JSX.Element {
		let { className, ...props } = this.props;

		// TODO: Restyle this so the user doesn't have to input the hours and minutes

		return (
			<input
				{...props}
				ref={elem => this._inputElem = elem}
				className={classnames('datetime-input form-control', className)}
				defaultValue="" // TODO: Fix so that the default value below displays in local time
				// defaultValue={this._normalizedDefaultValue}
				type="datetime-local"
			/>
		);
	}

	private normalizeDateValue(value: string): string | undefined {
		try {
			const date = new Date(value).toISOString();

			return date.substring(0, date.length - 1);
		} catch {
			return undefined;
		}
	}
}