﻿import classnames from 'classnames';
import moment from 'moment';
import * as React from 'react';

import { Loading } from '../loading/loading';
import './styles.scss';

export interface ProductListProps extends React.HTMLAttributes<HTMLElement> {
    items: ProductListItemData[];
}

export const ProductList = ({ className, items, ...props }: ProductListProps) => (
    <table
        {...props}
        className={classnames('product-list table', className)}
    >
        <thead>
            <tr className="product-list-header">
                <th className="product-list-item-id" scope="col">
                    Id
                </th>
                <th className="product-list-item-name" scope="col">
                    Name
                </th>
                <th className="product-list-item-stock" scope="col">
                    In-Stock
                </th>
                <th className="product-list-item-arrival" scope="arrival">
                    Arrival
                </th>
            </tr>
        </thead>
        <tbody>
            {items.map(item => (
                <ProductListItem
                    key={item.id}
                    item={item}
                />
            ))}
        </tbody>
    </table>
);

export interface ProductListItemData {
    arrival: string | null;
    id: number;
    name: string;
    stock: number;
}

interface ProductListItemProps {
    item: ProductListItemData;
}

export const ProductListItem = ({ item }: ProductListItemProps) => (
    <tr className="product-list-item">
        <td className="product-list-item-id">
            {item.id}
        </td>
        <td className="product-list-item-name">
            {item.name}
        </td>
        <td className="product-list-item-stock">
            {item.stock}
        </td>
        <td className="product-list-item-arrival">
            {/* TODO: Make sure times display in local timezones */}
            {item.arrival
                ? moment(item.arrival).format('MM/DD/YYYY')
                : <Loading className="product-list-loading" />
            }
        </td>
    </tr>
);