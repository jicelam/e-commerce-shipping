import classnames from 'classnames';
import * as React from 'react';

import './styles.scss';

export interface LoadingProps extends React.HTMLAttributes<HTMLElement> { }

export const Loading = ({ className, ...props }: LoadingProps) => (
	<div
		{...props}
		className={classnames('loading', className)}
	>
		<LoadingIcon />
	</div>
);

export interface LoadingIconProps extends React.HTMLAttributes<HTMLElement> { }

export const LoadingIcon = ({ className, role, ...props }: LoadingIconProps) => (
	<div
		{...props}
		className={classnames('loading-icon spinner-border', className)}
		role={classnames('status', role)}
	>
		<span className="sr-only">
			Loading...
		</span>
	</div>
);