import classnames from 'classnames';
import * as React from 'react';

import './styles.scss';

export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {}

export const Button = ({ children, className, ...props }: ButtonProps) => (
	<button
		{...props}
		className={classnames('btn btn-primary', className)}
		type="button"
	>
		{children}
	</button>
);