﻿import { ProductData } from 'data';

export interface GetProductsParams {
    orderDate?: Date;
}

export async function getProducts({ orderDate }: GetProductsParams = {}): Promise<ProductData[]> {
    const response = await fetch(`api/products${orderDate ? `?orderDate=${orderDate.toISOString()}` : ''}`);

    await checkForErrors(response);

    return await response.json() as ProductData[];
}

async function checkForErrors(response: Response): Promise<void> {
    if (200 <= response.status && response.status <= 300) {
        return;
    }

    let errorMessage: string | null = null;
    try {
        const error = await response.json() as ApiError;

        errorMessage = error && error.message ? error.message : null;
    } catch {
        // Ignore improperly formatted errors and use default below
    }

    throw new Error(errorMessage || 'Something went wrong');
}

interface ApiError {
    message: string;
}