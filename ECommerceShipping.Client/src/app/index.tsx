import 'bootstrap/dist/css/bootstrap.css';
import * as React from 'react';

import {
    Button,
    DatetimeInput,
    Loading,
    PrependedFormGroup,
    ProductList,
    ProductListItemData
} from 'components';
import { ProductData } from 'data';
import { getProducts } from 'services';
import './styles.scss';

export interface AppProps { }

interface AppState {
    error: string | null;
    loading: boolean;
    products: ProductListItemData[] | null;
    recalculatingArrival: boolean;
    submitDisabled: boolean;
}

export class App extends React.Component<AppProps, AppState> {
    private _dateInputElem: DatetimeInput | null = null;

    constructor(props: AppProps) {
        super(props);

        this.state = {
            error: null,
            loading: true,
            products: null,
            recalculatingArrival: false,
            submitDisabled: true
        };

        this.setDateInputElem = this.setDateInputElem.bind(this);
        this.onDateInputChange = this.onDateInputChange.bind(this);
        this.onRecalculateClick = this.onRecalculateClick.bind(this);
    }

    render(): JSX.Element {
        const { loading } = this.state;

        return (
            <div className="app">
                {loading
                    ? <Loading className="app-loading" />
                    : this.renderProducts()
                }
            </div>
        );
    }

    async componentDidMount(): Promise<void> {
        try {
            this.setState({ loading: true });
    
            const products = await getProducts();
    
            this.setState({
                loading: false,
                products: this.convertProductData(products)
            });
        } catch (error) {
            this.setState({
                error: error.message,
                loading: false,
                submitDisabled: false
            });
        }
    }

    private renderProducts(): JSX.Element {
        const { error, products, submitDisabled } = this.state;

        var startingDate = new Date().toISOString();

        return (
            <div className="app-product-list-view">
                <div className="app-product-list-body">
                    {error
                        ? <div className="app-product-list-error text-danger">
                            {error}
                        </div>
                        : <>
                            <label className="app-product-list-label">
                                Current Products
                            </label>
                            <div className="app-product-list-container">
                                <ProductList items={products!} />
                            </div>
                        </>
                    }
                </div>
                <div className="app-product-list-input-footer container">
                    <div className="row">
                        <PrependedFormGroup className="col-9" text="Order Date">
                            <DatetimeInput
                                ref={this.setDateInputElem}
                                defaultValue={startingDate.substr(0, startingDate.length - 1)}
                                onChange={this.onDateInputChange}
                            />
                        </PrependedFormGroup>
                        <Button
                            className="app-product-list-recalculate-button col-3"
                            disabled={submitDisabled}
                            title="Recalculate"
                            onClick={this.onRecalculateClick}
                        >
                            Recalculate
                        </Button>
                    </div>
                </div>
            </div>
        );
    }

    private setDateInputElem(elem: DatetimeInput | null): void {
        this._dateInputElem = elem;
    }

    private onDateInputChange(event: React.ChangeEvent<HTMLInputElement>): void {
        try {
            const result = Date.parse(event.target.value);
            if (isNaN(result))
                throw new Error();

            this.setState({ submitDisabled: false });
        } catch {
            this.setState({ submitDisabled: true });
        }
    }

    private async onRecalculateClick(): Promise<void> {
        try {
            const { products } = this.state;
            if (products) {
                for (let product of products!) {
                    product.arrival = null;
                }
            }

            /*
             * TODO: If a 500 error comes through that prevents products
             * from loading at all and the user clicks 'Recalculate', a
             * loading icon should show in the list body while the input
             * row remains visible
             */
    
            this.setState({
                products: products,
                submitDisabled: true
            });
    
            const dateValue = this._dateInputElem!.value;
            const updatedProducts = await getProducts({
                orderDate: dateValue ? new Date(dateValue) : undefined
            });
    
            this.setState({
                error: null,
                products: this.convertProductData(updatedProducts),
                submitDisabled: false
            });
        } catch (error) {
            this.setState({
                error: error.message,
                products: null,
                submitDisabled: false
            })
        }
    }

    private convertProductData(products: ProductData[]): ProductListItemData[] {
        return products.map(product => ({
            arrival: product.estimatedArrival,
            id: product.id,
            name: product.name,
            stock: product.inventoryQuantity
        }));
    }
}