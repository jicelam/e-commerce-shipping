﻿using ECommerceShipping.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ECommerceShipping.Data.Services
{
    public interface IECommerceShippingDataService
    {
        Task<IEnumerable<ProductModel>> GetProducts();
    }

    public class EntityECommerceShippingDataService : IECommerceShippingDataService, IDisposable
    {
        private readonly ECommerceShippingContext _context;

        public EntityECommerceShippingDataService(ECommerceShippingContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        public async Task<IEnumerable<ProductModel>> GetProducts()
        {
            // TODO: Add paging support
            return await _context.Products.ToListAsync();
        }
    }
}
