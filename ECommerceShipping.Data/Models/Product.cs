﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;

namespace ECommerceShipping.Data.Models
{
    public class ProductModel
    {
        public static void ApplyAttributes(EntityTypeBuilder<ProductModel> builder)
        {
            builder.HasKey(e => e.Id);

            // TODO: Add indexes as needed
        }

        public int Id { get; set; }

        public int InventoryQuantity { get; set; }

        public int MaxBusinessDaysToShip { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1)]
        public string Name { get; set; }

        public bool ShipOnWeekends { get; set; }
    }
}
