﻿using ECommerceShipping.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ECommerceShipping.Data
{
    public class ECommerceShippingContext : DbContext
    {
        public DbSet<ProductModel> Products { get; set; }

        public ECommerceShippingContext(DbContextOptions<ECommerceShippingContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductModel>(ProductModel.ApplyAttributes);
        }
    }
}
