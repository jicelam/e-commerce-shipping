﻿using ECommerceShipping.Api.Controllers.Responses;
using ECommerceShipping.Api.Dtos;
using ECommerceShipping.Data.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerceShipping.Api.Controllers
{
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IECommerceShippingDataService _dataService;

        public ProductsController(IECommerceShippingDataService dataService)
        {
            _dataService = dataService;
        }

        [HttpGet]
        public async Task<ActionResult<ProductDto>> Index(
            DateTime? orderDate = null
        )
        {
            try
            {
                // Not part of the spec, but convenient to use for checking error handling
                //if (orderDate != null && orderDate.Value.Date < DateTime.Today)
                //    return BadRequest(new Responses.ErrorResponse("Please pass an order date that is today or later"));

                var products = await _dataService.GetProducts();

                return Ok(products.Select(
                    product => new ProductDto(product, new ProductEstimatedArrivalOptions(orderDate))
                ));
            }
            catch
            {
                // TODO: Set up exception handling such that all api controllers return standard JSON response on error
                return StatusCode(500, new ErrorResponse());
            }
        }
    }
}
