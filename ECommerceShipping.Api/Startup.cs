﻿using ECommerceShipping.Api.Data;
using ECommerceShipping.Data;
using ECommerceShipping.Data.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ECommerceShipping.Api
{
    public class Startup
    {
        private const string _corsPolicyName = "Standard";

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddDbContext<ECommerceShippingContext>(options =>
                {
                    options.UseSqlServer(Configuration.GetConnectionString("ECommerceShippingDB"));
                })
                .AddTransient<IECommerceShippingDataService, EntityECommerceShippingDataService>()
#if DEBUG
                .AddCors(options =>
                {
                    options.AddPolicy(_corsPolicyName, builder =>
                    {
                        builder.WithOrigins("http://localhost:5532");
                    });
                })
#endif
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "wwwroot";
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // TODO: Add logging configuration

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
#if DEBUG
            app.UseCors(_corsPolicyName);
#endif

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "../ECommerceShipping.Client";
            });

#if DEBUG
            // Run database migrations, then seed it if it didn't previously exist
            var serviceScope = app.ApplicationServices.CreateScope();
            var dbContext = serviceScope.ServiceProvider.GetService<ECommerceShippingContext>();

            var dbAlreadyExists = (dbContext.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists();
            dbContext.Database.Migrate();

            if (!dbAlreadyExists)
            {
                dbContext.Seed("Data/Samples");
            }
#endif
        }
    }
}
