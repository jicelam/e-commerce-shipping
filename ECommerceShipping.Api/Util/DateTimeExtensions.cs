﻿using System;

namespace ECommerceShipping.Api.Util
{
    public static class DateUtilities
    {
        public static DateTime AddBusinessDays(this DateTime startDate, int businessDays, bool includeWeekends = false)
        {
            // TODO: Get working with 0 and negative values
            if (businessDays < 1)
                throw new ArgumentOutOfRangeException("AddBusinessDays currently only supports adding values of 1 or greater");

            if (includeWeekends)
                return startDate.AddDays(businessDays - 1);

            var daysFromSunday = (int)startDate.DayOfWeek;
            var adjustedMaxDays = businessDays + daysFromSunday - (daysFromSunday > 0 ? 1 : 0);

            var daysToAdd = adjustedMaxDays + (Math.Ceiling((double)adjustedMaxDays / 5) - 1) * 2;

            return startDate.AddDays(-daysFromSunday + daysToAdd);
        }
    }
}
