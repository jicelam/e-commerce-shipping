﻿using ECommerceShipping.Api.Util;
using ECommerceShipping.Data.Models;
using System;

namespace ECommerceShipping.Api.Dtos
{
    public class ProductDto
    {
        public DateTime EstimatedArrival { get; private set; }
        public int Id { get; private set; }
        public int InventoryQuantity { get; private set; }
        public int MaxBusinessDaysToShip { get; private set; }
        public string Name { get; private set; }
        public bool ShipOnWeekends { get; private set; }

        public ProductDto(
            ProductModel model,
            ProductEstimatedArrivalOptions estimatedArrivalOptions = new ProductEstimatedArrivalOptions()
        )
        {
            // TODO: Set these up with Automapper
            Id = model.Id;
            InventoryQuantity = model.InventoryQuantity;
            MaxBusinessDaysToShip = model.MaxBusinessDaysToShip;
            Name = model.Name;
            ShipOnWeekends = model.ShipOnWeekends;

            EstimatedArrival = estimatedArrivalOptions.StartDate.AddBusinessDays(MaxBusinessDaysToShip, ShipOnWeekends);
        }
    }

    public struct ProductEstimatedArrivalOptions
    {
        public DateTime StartDate { get; private set; }

        public ProductEstimatedArrivalOptions(
            DateTime? startDate = null
        )
        {
            StartDate = startDate ?? DateTime.Today.ToUniversalTime();
        }
    }
}
