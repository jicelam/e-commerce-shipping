﻿using ECommerceShipping.Data;
using ECommerceShipping.Data.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace ECommerceShipping.Api.Data
{
#if DEBUG
    public static class ContextExtensions
    {
        public static void Seed(this ECommerceShippingContext context, string sampleFolderPath)
        {
            try
            {
                using (var sampleProductsStream = File.OpenRead($"{sampleFolderPath}/products.json"))
                {
                    // TODO: Parse this in chunks rather than all at once in case sample input is very large
                    var sampleProducts = JToken.ReadFrom(new JsonTextReader(new StreamReader(sampleProductsStream)));

                    foreach (var sampleProduct in sampleProducts)
                    {
                        var product = new ProductModel
                        {
                            InventoryQuantity = sampleProduct.Value<int>("inventoryQuantity"),
                            MaxBusinessDaysToShip = sampleProduct.Value<int>("maxBusinessDaysToShip"),
                            Name = sampleProduct.Value<string>("productName"),
                            ShipOnWeekends = sampleProduct.Value<bool>("shipOnWeekends")
                        };

                        context.Products.Add(product);
                    }

                    context.SaveChanges();
                }
            }
            catch (Exception error)
            {
                throw new InvalidDataException("The provided sample data was either not found or presented in an improper format", error);
            }
        }
    }
#endif
}
