# ECommerceShipping

ECommerceShipping is small web app that displays a single page that allows users to see the projected arrival date for a list of products given a particular starting date. The app functionality itself is very simple, and the project easily employs more technologies than needed to achieve all that. The project's ultimate goal is to demonstrate competency with a variety of full-stack technologies, so a little over-engineering is to be expected given the simple requirements.

## Application Structure

The ECommerceShipping solution is divided into a number of smaller projects based on typical separation of concerns common to a web application. The current projects are as follows:

- ECommerceShipping.Api: The core project that spins up the web api
- ECommerceShipping.Api.Test: Tests for code in ECommerceShipping.Api (appropriately enough)
- ECommerceShipping.Client: The app's browser-based front-end
- ECommerceShipping.Data: The data access layer

## Application Dependencies

ECommerceShipping assumes the following dependencies:

- .NET Core SDK 2.2
- Node (The version used for all of development was 12.7.0, but a lower one that can still run webpack should do)
- SQL Server (The built-in Visual Studio version should be fine, but if not, try downloading the Development version of SQL Server Management Studio)

## Building the Solution

The easiest way to build and run the ECommerceShipping app is from Visual Studio. Open the solution, make sure that ECommerceShipping.Client project has restored its dependencies (e.g. by right clicking on the npm listing in the Solution Explorer and running "Install Missing npm Packages") and then run the debugger. If the browser does not automatically appear, please open one of your choosing and navigate to https://localhost:44386/. Note that the app currently does not work in Internet Explorer, and no testing has been done for Safari, Opera or any mobile browsers.

The app can also be built and run from the command line. The web api displays static files generated from the client project, so the client needs to be built first. Navigate to the ECommerceShipping.Client project root and run

```
npm install
```

Then

```
npm run build
```

Once all that finishes, navigate to ECommerceShipping.Api project root and run

```
dotnet run
```

Then, navigate to https://localhost:5001 in the browser. The preferred development environments thus far have been Visual Studio for the API project, and Visual Studio Code for the client.

## Testing

A few unit tests currently only exist in the back-end for a couple non-trivial pieces of code. They utilize nUnit, and can be run either from the Visual Studio Test Explorer, or from the command line at the ECommerceShipping.Api.Test project root with

```
dotnet test
```

# Project Details

## ECommerceShipping.Api

ECommerceShipping.Api is a basic ASP.NET Core project that mostly concerns itself with setting up API endpoints, since bulk of the display logic happens with the client. All endpoint definitions are found in the Controllers folder, and these call out into the rest of the application's back-end infrastructure. Sample data is currently stored with SQL Server and accessed through Entity Framework Core in the ECommerceShipping.Data project.

## ECommerceShipping.Client

ECommerceShipping.Client contains most of the application's front-end UI. It is built to function as an SPA that communicates with the back-end through HTTP requests. As this is mostly a demo project, little attention has been paid to concerns around SEO, code-splitting, tree-shaking and other such optimizations that are needed for most production-ready SPAs.

TypeScript is used for all front-end (build process code and a few utility scripts still use plain old JS however). TSLint has also been set up and integrated into the project's build chain.

The project uses React to handle all of the presentation, and all the content is rendered dynamically. Since the UI at this point is still very slight, the client makes basic use of the built-in React state handling over common solutions like Redux for the sake of simplicity. As this author does not yet have much experience with React hooks, class components with setState are used instead. Although Redux or similar libraries are not used, efforts are still made to maintain the common distinction between state-aware "smart" components (currently with only one component contained in the src/app folder) and stage-agnostic "dumb" components (located in the src/components folder).

SASS is used as the CSS preprocessor, and the project also pulls in Bootstrap for some basic component styling and themes. The project does not make use of any major CSS coding frameworks like BEM or SMACSS, though the goal has been to keep scss styles split into separate files housed in the same folder as their related UI components. CSS modules are not currently enabled in the build process, so styles are still global.

Webpack handles the project build process. Although the sibling API project is set up to run on its own, the client project has been set up to run with Webpack Dev Server to allow for more rapid UI development. This can be activated by first spinning up the API project, then navigating to the ECommerceShipping.Client project root in the command line and running

```
npm start
```

The development server can be accessed from the browser at http://localhost:5532. The project also has a watch mode, which allows changes to show up on page refresh even if only the API project is running. Start this by navigating to the project root and running

```
npm run watch
```