﻿using ECommerceShipping.Api.Util;
using System;
using System.Collections.Generic;
using Xunit;

namespace ECommerceShipping.Api.Test.Util
{
    public class AddBusinessDaysTests
    {
        [Fact]
        public void InputInOneWeek_Success()
        {
            ExecuteTests(new List<DateTimeTestData>
            {
                new DateTimeTestData(new DateTime(2019, 8, 6), 3, new DateTime(2019, 8, 8)),
                new DateTimeTestData(new DateTime(2019, 7, 29), 5, new DateTime(2019, 8, 2)),
                new DateTimeTestData(new DateTime(2019, 8, 9), 1, new DateTime(2019, 8, 9)),
                new DateTimeTestData(new DateTime(2019, 8, 2), 1, new DateTime(2019, 8, 2))
            });
        }

        [Fact]
        public void InputAcrossWeeks_Success()
        {
            ExecuteTests(new List<DateTimeTestData>
            {
                new DateTimeTestData(new DateTime(2019, 7, 30), 7, new DateTime(2019, 8, 7)),
                new DateTimeTestData(new DateTime(2019, 7, 23), 19, new DateTime(2019, 8, 16)),
                new DateTimeTestData(new DateTime(2019, 6, 27), 38, new DateTime(2019, 8, 19)),
                new DateTimeTestData(new DateTime(2019, 8, 2), 2, new DateTime(2019, 8, 5))
            });
        }

        [Fact]
        public void InputAcrossYears_Success()
        {
            ExecuteTests(new List<DateTimeTestData>
            {
                new DateTimeTestData(new DateTime(2019, 12, 30), 4, new DateTime(2020, 1, 2)),
                new DateTimeTestData(new DateTime(2019, 12, 31), 9, new DateTime(2020, 1, 10))
            });
        }

        [Fact]
        public void InputStartingOnWeekend_Success()
        {
            ExecuteTests(new List<DateTimeTestData>
            {
                new DateTimeTestData(new DateTime(2019, 8, 4), 1, new DateTime(2019, 8, 5)),
                new DateTimeTestData(new DateTime(2019, 8, 3), 1, new DateTime(2019, 8, 5)),
                new DateTimeTestData(new DateTime(2019, 7, 27), 4, new DateTime(2019, 8, 1)),
                new DateTimeTestData(new DateTime(2019, 7, 14), 8, new DateTime(2019, 7, 24)),
                new DateTimeTestData(new DateTime(2019, 7, 13), 8, new DateTime(2019, 7, 24))
            });
        }

        [Fact]
        public void InputAcrossWeeks_IncludeWeekends_Success()
        {
            ExecuteTests(new List<DateTimeTestData>
            {
                new DateTimeTestData(new DateTime(2019, 7, 30), 7, new DateTime(2019, 8, 5)),
                new DateTimeTestData(new DateTime(2019, 7, 23), 19, new DateTime(2019, 8, 10)),
                new DateTimeTestData(new DateTime(2019, 6, 27), 38, new DateTime(2019, 8, 3)),
                new DateTimeTestData(new DateTime(2019, 8, 2), 2, new DateTime(2019, 8, 3)),
                new DateTimeTestData(new DateTime(2019, 7, 27), 6, new DateTime(2019, 8, 1)),
                new DateTimeTestData(new DateTime(2019, 7, 26), 2, new DateTime(2019, 7, 27)),
                new DateTimeTestData(new DateTime(2019, 7, 27), 1, new DateTime(2019, 7, 27)),
                new DateTimeTestData(new DateTime(2019, 12, 31), 6, new DateTime(2020, 1, 5))
            }, true);
        }

        [Fact]
        public void InvalidInput_Failure()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                ExecuteTests(new List<DateTimeTestData> { new DateTimeTestData(new DateTime(2019, 7, 24), 0, new DateTime(2019, 7, 24)) });
            });
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                ExecuteTests(new List<DateTimeTestData> { new DateTimeTestData(new DateTime(2019, 8, 9), -6, new DateTime(2019, 8, 1)) });
            });
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                ExecuteTests(new List<DateTimeTestData> { new DateTimeTestData(new DateTime(2019, 8, 9), -1, new DateTime(2019, 8, 8)) });
            });
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                ExecuteTests(new List<DateTimeTestData> { new DateTimeTestData(new DateTime(2019, 7, 24), 0, new DateTime(2019, 7, 24)) }, true);
            });
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                ExecuteTests(new List<DateTimeTestData> { new DateTimeTestData(new DateTime(2019, 8, 9), -6, new DateTime(2019, 8, 1)) }, true);
            });
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                ExecuteTests(new List<DateTimeTestData> { new DateTimeTestData(new DateTime(2019, 8, 9), -1, new DateTime(2019, 8, 8)) }, true);
            });
        }

        private void ExecuteTests(List<DateTimeTestData> testItems, bool includeWeekends = false)
        {
            foreach (var testItem in testItems)
            {
                var endDate = testItem.StartDate.AddBusinessDays(testItem.BusinessDaysToAdd, includeWeekends);

                Assert.True(endDate == testItem.ExpectedEndDate, $"Expected: {testItem.ExpectedEndDate.ToString("MM/dd/yyyy")}, Actual: {endDate.ToString("MM/dd/yyyy")}");
            }
        }

        private struct DateTimeTestData
        {
            public DateTime StartDate { get; private set; }
            public int BusinessDaysToAdd { get; private set; }
            public DateTime ExpectedEndDate { get; private set; }

            public DateTimeTestData(
                DateTime startingDate,
                int businessDaysToAdd,
                DateTime expectedEndDate
            )
            {
                StartDate = startingDate;
                BusinessDaysToAdd = businessDaysToAdd;
                ExpectedEndDate = expectedEndDate;
            }
        }
    }
}
